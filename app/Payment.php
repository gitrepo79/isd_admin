<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Payment extends Model
{
    
    protected  $table = "payment";

    public static function boot()
    {
        parent::boot();
        static::creating(function($model) {
            $user = User::find($model->client_id);
            $model->client_email  = $user->email;
            $model->client_mobile  = $user->contacts;
            $model->client_name = $user->name;
        });        

    }

    public function subscription(){
        return $this->belongsTo('App\Subscription','book_id');   
    }   
    
    
    protected $statuses = array(
        '1' => 'Cash',
        '2' => 'Bank Transfer',
        '3' => 'Client wallet',
        '4' => 'Credit Card',
        '5' => 'Cash / Credit Card'
    );

    public function getTypeAttribute($value)
    {
        return $this->statuses[$value];
    }
    

}
