<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Booking extends Mailable
{
    use Queueable, SerializesModels;
    public $details;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        //
        $this->details  = $details;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $details = $this->details ;
        return $this->subject('Thank You For Booking')->view('emails.thankyou')
        ->attach(public_path('attached/tax_'.$details['id'].'.pdf'), [
                         'as' => 'tax_'.$details['id'].'.pdf',
                         'mime' => 'application/pdf',
                    ])
        ->attach(public_path('attached/invoice_'.$details['id'].'.pdf'), [
             'as' => 'invoice_'.$details['id'].'.pdf',
             'mime' => 'application/pdf',
        ]);
    }
}
