<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title> Pay Order </title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <link rel="stylesheet" href="/paymobileapp/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="/paymobileapp/assets/css/normalize.css">
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/paymobileapp/assets/css/fontello.css">
        <link rel="stylesheet" href="/paymobileapp/assets/css/style.css">
        <!--<link rel="stylesheet" href="assets/css/style2.css">-->
        <link rel="stylesheet" href="https://designmodo.com/demo/creditcardform/style.css">
        <style>
            .form-group {
                margin-bottom: 0 !important;
            }            
        </style>
    </head>
    <body>
       <header>
            <div class="logo">
                <img src="../../../../public/logo.png" style="width:50px;">
            </div>
        </header>
        <div class="wrapper">

    <section class="payment-method">
        <ul>
             <li>
                <div class="details" >
                    
                    <h3>Payment Card Details</h3>
                    <form action="http://bookings.isddubai.com/api/v1/pay/agian" class="form-horizontal">
  
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label>CVV</label>
                                <input type="text" class="form-control" name="cvv"  id="payfort_fort_mp2_cvv" maxlength="4">
                                <input type="hidden"  name="id" value="<?=$_GET['id']?>">
                                <input type="hidden"  name="amount" value="<?=$_GET['amount']?>">
                                <input type="hidden"  name="client_id" value="<?=$_GET['client_id']?>">
                                <input type="hidden"  name="rent" value="<?=$_GET['rent']?>">
                                <input type="hidden"  name="method" value="<?=$_GET['method']?>">
                            </div>
                        </div>
                        <section class="actions">
                            <input type="submit" value="Pay" class="continue" id="btn_continue" />
                        </section>                        
                    </form>
    
                </div>
            </li>
        </ul>
    </section>


    <script type="text/javascript" src="/paymobileapp/vendors/jquery.min.js"></script>
        <script type="text/javascript" src="/paymobileapp/assets/js/jquery.creditCardValidator.js"></script>
        <script type="text/javascript" src="/paymobileapp/assets/js/checkout.js"></script>
        <script type="text/javascript">
		  
		
            $(document).ready(function () {
				
				
                //getPaymentPage("cc_merchantpage");
				var remember_me ="NO";
                $('input[type="checkbox"]').click(function(){
                    if($(this).prop("checked") == true){
                        remember_me ="YES";
                        console.log("YES");

                    }
                    else if($(this).prop("checked") == false){
                        remember_me ="NO";
                        console.log("NO");
                    }
                });


                $('input:radio[name=payment_option]').click(function () {
                    $('input:radio[name=payment_option]').each(function () {
                        if ($(this).is(':checked')) {
                            $(this).addClass('active');
                            $(this).parent('li').children('label').css('font-weight', 'bold');
                            $(this).parent('li').children('div.details').show();
                        }
                        else {
                            $(this).removeClass('active');
                            $(this).parent('li').children('label').css('font-weight', 'normal');
                            $(this).parent('li').children('div.details').hide();
                        }
                    });
                });
                $('#btn_continue').click(function () {
                    var paymentMethod = "cc_merchantpage2";
                    if(paymentMethod == 'cc_merchantpage2') {
                        var isValid = payfortFortMerchantPage2.validateCcForm();
                        if(isValid) {
                            getPaymentPage(paymentMethod,remember_me);
                        }
                    }
                    else{
                        getPaymentPage(paymentMethod);
                    }
                });
            });
        </script>
        </div>
        <footer>
        <script>
        document.body.addEventListener("wheel", e=>{
            if(e.ctrlKey)
            e.preventDefault();//prevent zoom
        });
        </script>
        </footer>
    </body>
</html>
