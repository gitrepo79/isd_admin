@extends('layouts.user')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    {!! Form::open(['route' => 'client.bookings.store', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data"]) !!}
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Bookings</h1>
    </div>
    
    <div class="row">
        <div class="col-lg-12">
            <!-- Default Card Example -->
            <div class="card mb-4">
                <div class="card-header">
                   <h6 class="m-0 font-weight-bold text-primary">Available Products</h6>
                </div>
                <div class="card-body">

                    <table class="table table-bordered" id="dataTable1" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Prodct Name</th>
                                <th>Rate</th>
                                <th>Quantity</th>
                                
                            </tr>
                        </thead>
                        
                        <tbody>
                            @foreach($products as $key=> $product)
                            <tr>
                                <td>
                                  
                                <input type="checkbox" name="products[]" value="{{$product->id}}">
                                
                                
                                </td>
                                <td>{{$product->title}}
                                    
                                    <td>
                                        {{$product->price}}
                                        
                                    </td>
                                    <td>
                                        <input type="number" name="qty[]" class="form-control">
                                    </td>
                                    
                                </tr>
                                @endforeach
                            </tbody>
                            
                        </table>
                        
                    </div>
                </div>
                
            </div>
        </div>
        <!-- DataTales Example -->
        @if(!empty($data))
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Available Courts</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable1" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Sno</th>
                                <th>Court Name</th>
                                <th>Rate</th>
                                
                            </tr>
                        </thead>
                        
                        <tbody>
                            @foreach($data as $dt)
                            <tr>
                                <td>
                                    <input type="radio" name="pitch" value="{{$dt->id}}">
                                </td>
                                
                                <td>Court Name:{{$dt->name}}
                                <br></td>
                                <td>
                                    
                                    {{$dt->rate}}
                                    
                                </td>
                                
                            </tr>
                            @endforeach
                        </tbody>
                        
                    </table>
                    <button type="submit" class="btn btn-primary">Proceed</button>
                    
                    
                </div>
            </div>
        </div>
        @endif
        {!! Form::close() !!}
    </div>
    <!-- /.container-fluid -->
    @endsection