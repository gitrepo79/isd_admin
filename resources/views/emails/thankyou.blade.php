<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Thank You - ISD</title>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100&display=swap" rel="stylesheet">

</head>

<body
    style="background: #ffffff; margin: 0; padding: 0; font-family: 'Roboto'; font-size: 16px; line-height: 1.6; color: #3e2b64;">
    <div style="margin: 0 auto; padding: 0px; width: 600px; max-width: 600px;">
        <table cellpadding="0"
            style="width: 600px; background: #ffffff; border-collapse: collapse; border-spacing: 0; max-width: 600px; margin: 0 auto;">
            <tbody>

                <tr>
                    <td colspan="2">
                        <a href="https://isddubai.com/" target="_blank" style="display: block;">
                            <img src="http://www.isddubai.com/emails/app-emails/thankyou/images/header.png"
                                alt="ISD Dubai" style="display: block; margin: 0 auto;">
                        </a>
                    </td>
                </tr>

                <tr>
                    <td colspan="2" style="padding: 20px 0; ">

                        <p
                            style="margin: 0 60px 20px; line-height: 1.4; font-size: 16px; font-family: Arial, sans-serif; color: #3e2b64;">

                            <strong style="font-size: 18px;">Thank you for your booking.</strong> <br><br>

                            You slot is confirmed with the below details:
                        </p>

                        <table
                            style="margin: 0 60px 40px; font-size: 16px; font-family: Arial, sans-serif; color: #3e2b64;">
                            <tr>
                                <td>Booking Sport:</td>
                                <?php $sports = DB::table('sports')
                                    ->where('id', $details['sports'])
                                    ->value('name'); ?>
                                <td>{{ $sports }}</td>
                            </tr>

                            <tr>
                                <td>Booking Ref. No.:</td>
                                <td>{{ $details['code'] }} </td>
                            </tr>

                            <tr>
                                <td>Booking Date:</td>
                                <td>{{ $details['date'] }}</td>
                            </tr>

                            <tr>
                                <td>Booking Time:</td>
                                <td>{{ $details['from'] }} - {{ $details['to'] }}</td>
                            </tr>

                            <tr>
                                <td>Pitch /Court Number:</td>
                                <?php $court = DB::table('courts')
                                    ->where('id', $details['pitch'])
                                    ->value('name'); ?>
                                <td>{{ $court }} {{ $details['pitch'] }}</td>
                            </tr>

                            <tr>
                                <td>Total Fee: AED</td>
                                <td>{{ $details['total'] }}</td>
                            </tr>
                        </table>

                        <p
                            style="margin: 0 60px 20px; line-height: 1.4; font-size: 16px; font-family: Arial, sans-serif; color: #3e2b64;">

                            We look forward to welcoming you at ISD. <br>

                            <strong style="font-size: 18px;">Call us on: +971 4 448 1555</strong>
                        </p>

                        <p
                            style="margin: 0 60px 0px; line-height: 1.4; font-size: 20px; font-family: 'Roboto', Arial, sans-serif; font-weight: 700; color: #3e2b64; text-transform: uppercase;">

                            isddubai.com

                        </p>

                    </td>
                </tr>

                <tr>

                    <td>

                        <img src="http://www.isddubai.com/emails/app-emails/thankyou/images/footer.png" alt="ISD Dubai"
                            style="display: block; margin: 0 auto;">

                    </td>
                </tr>

            </tbody>
        </table>
    </div>
</body>

</html>
