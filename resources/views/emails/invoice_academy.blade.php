<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;500;700&family=Roboto:wght@100;300;400;700&display=swap" rel="stylesheet">

    <style>
       .main {
            font-family: 'Roboto' !important;
            padding: 0px 30px;
            font-size:12px;
        }

        .title {
            font-family: 'Oswald', sans-serif !important;
            font-size:14px;
        }   
        
        .title2 {
            font-family: 'Oswald', sans-serif !important;
            font-size:24px;
        } 
        
        p {
            margin: 2.5px;
        }
        table, th, td {
            border: 1px solid black;
        }
        td {
            padding: 10px;
        }
    </style>
</head>
    
    <?php  $subscription = App\Subscription::where('id',$details['id'])->with('classes')->first();  ?>
    
<body class="main" style="padding: 30px;">
<div style="width: 100%; display:inline-block;margin-bottom: 40px;">
    <div style="float: left;"><img src="https://isddubai.com/assets-web/images/logos/isddubai-color.svg" height="120" ></div>
    <div style="float: right"><p>Inspiratus Consulting Limited Dubai Br</p><p>Dubai Silicon Oasis</p><p>Suntech Tower, office 904</p><p>+971 4 333 9130</p><p>laligacademy@inspirat.us</p><p>TRN#100268867700003</p></div>
</div>
<div style="clear:both;"></div>
<div class="title2" style="width: 100%; margin: 20px;padding-top: 20px; display:inline-block; text-align: center; color: #ed2524">TAX INVOICE</div>
<div style="clear:both;"></div>
<div style="width: 100%; display:inline-block; padding-top: 20px;"> 
    <div style="float: left; border: 1px #000 solid; padding: 20px;"><p style="padding-bottom: 10px;">Bill to:</p><p><b>{{ $subscription->parentName->name}}</b></p><p>For: {{ $subscription->subscriber}}</p><p>Email Address: {{ $subscription->parentName->email}}</p></div>
    <div style="float: right; border: 1px #000 solid; padding: 20px;"><p><b>Date:</b> {{ \Carbon\Carbon::parse($subscription->created_at)->format('d M Y') }} </p><p><b>Invoice #</b> {{$details['id']}}</p><p><b>Academy:</b> {{ $subscription->getAcademyNameAttribute($subscription->academy)}}</p></div>
</div>
<div style="clear:both;"></div>
<div style="width: 100%; display: inline-block; padding-top: 20px;">
<table style="width: 100%;" cellspacing="0">
    <tr class="title" style="background-color:#ed2524; color: #fff; font-weight: bolder;">
        <td>Description</td>
        <td>Quantity</td>
        <td>Unit Price</td>
        <td>Total AED</td>
    </tr>
    <tr class="main">
        <td>
            <p>Player Name :{{ $subscription->subscriber}}</p>
            <p>Age category : {{ $subscription->getAgeAttribute($subscription->age)}}</p>
            <p>Package: {{ $subscription->packages->package_name}}</p>
            <p>Classes: @foreach($subscription->classes as $items) {{ $items->title }} , @endforeach</p>
            <p>Number of Training Days: {{ $subscription->type_num_days}}</p>
            <p>Start Date: {{ \Carbon\Carbon::parse($subscription->start_date)->format('d M Y') }}</p>
            <p>Applied Discount: {{ $subscription->discount}}  AED </p>
        </td>
        <td>1</td>
        <td>AED {{ round($subscription->subscription_fees / 1.05 ,2)}}</td>
        <td>AED {{ round($subscription->subscription_fees / 1.05 ,2)}}</td>
    </tr>
    @foreach($subscription->payments as $items)
    <tr>
        <td colspan="3">Payment on {{ \Carbon\Carbon::parse($subscription->created_at)->format('d M Y') }} ( {{ $items->type }} )</td>
        <td>AED {{$items->amount}}</td>
    </tr>
    @endforeach
    <tr>
        <td colspan="3">VALUE ADDED TAX - VAT (5%)</td>
        <td>AED {{ $subscription->subscription_fees - round($subscription->subscription_fees / 1.05 ,2) }}</td>
    </tr>
    <tr style="color: #fff; background-color: #412665; font-weight: bolder;">
        <td colspan="3">Total</td>
        <td>AED {{ $subscription->subscription_fees}}</td>
    </tr>
    <tr style="color: #fff; background-color: #412665; font-weight: bolder;">
        <td colspan="3">Advance</td>
        <td>AED {{ $subscription->payments->sum('amount')}}</td>
    </tr>
    <tr style="color: #fff; background-color: #412665; font-weight: bolder;">
        <td colspan="3">Balance Due</td>
        <td>AED {{ $subscription->subscription_fees - $subscription->payments->sum('amount') }}</td>
    </tr>
</table>

</div>
<div style="clear:both;"></div>
<div style="width: 100%; display: inline-block; padding-top: 30px; border: 1px solid #000;">
    <div style="padding: 20px;">
        <p><b><u>Terms & Conditions</u></b></p>
        <ul>
            <li>This payment is non-refundable</li>
            <li>Places are limited and are only guaranteed upon payment of this invoice and based on space availability at the time of payment</li>
            <li>Payment is required prior to commencement of each term</li>
            <li>Selection for Advanced Development and HPC Teams will incur additional costs which will be invoiced separately</li>
            <li>Participation in tournaments and leagues will incur additional participation and match kit fees which will be invoiced separately</li>
            <li>Missed classes cannot be re-scheduled. (No makeup sessions)</li>
            <li>If choosing to pay by bank transfer, please make sure to indicate under payment details: “LaLiga Academy – Name of Player” and share the transfer confirmation with us so we can track your payment</li>
        </ul>
    </div>
</div>

</body>
</html>