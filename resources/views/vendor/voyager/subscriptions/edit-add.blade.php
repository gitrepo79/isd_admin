@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
@endphp

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .custom_id,.custom_id2 {
            display: none;
        } 
        .panel-body {
            padding: 15px 0 !important;
            padding-bottom: 0 !important;
        }          
.select2-container {
    width: 100% !important;
}        
    </style>
@stop
@if($edit)
<style>
    .custom_id,.custom_id2 {
        display: block !important;
    }  
    .next{
        display: none !important;
    }
</style>
@endif
@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form" id="formdata"
                            class="form-edit-add"
                            action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('voyager.'.$dataType->slug.'.store') }}"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if($edit)
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{($edit ? 'editRows' : 'addRows' )};
                            @endphp

                            @foreach($dataTypeRows as $row)
                                <!-- GET THE DISPLAY OPTIONS -->
                                @php
                                    $display_options = $row->details->display ?? NULL;
                                    if ($dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')}) {
                                        $dataTypeContent->{$row->field} = $dataTypeContent->{$row->field.'_'.($edit ? 'edit' : 'add')};
                                    }
                                @endphp
                                @if (isset($row->details->legend) && isset($row->details->legend->text))
                                    <legend class="text-{{ $row->details->legend->align ?? 'center' }}" style="background-color: {{ $row->details->legend->bgcolor ?? '#f0f0f0' }};padding: 5px;">{{ $row->details->legend->text }}</legend>
                                @endif

                                <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width ?? 12 }} {{ $errors->has($row->field) ? 'has-error' : '' }} @if(isset($display_options->id)){{ $display_options->id }}@endif " @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif @if(isset($display_options->class)){{ "class=$display_options->id" }}@endif>
                                    {{ $row->slugify }}
                                    <label class="control-label" for="name">{{ $row->getTranslatedAttribute('display_name') }}</label>
                                    @include('voyager::multilingual.input-hidden-bread-edit-add')
                                    @if (isset($row->details->view))
                                        @include($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $dataTypeContent->{$row->field}, 'action' => ($edit ? 'edit' : 'add'), 'view' => ($edit ? 'edit' : 'add'), 'options' => $row->details])
                                    @elseif ($row->type == 'relationship')
                                        @include('voyager::formfields.relationship', ['options' => $row->details])
                                    @else
                                        {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                    @endif

                                    @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                        {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                    @endforeach
                                    @if ($errors->has($row->field))
                                        @foreach ($errors->get($row->field) as $error)
                                            <span class="help-block">{{ $error }}</span>
                                        @endforeach
                                    @endif
                                </div>
                            @endforeach
                            <div class="form-group  col-md-12 fees hidden ">
                                <label class="control-label" for="name">Subscription Fees Discount</label>
                                <input type="text" class="form-control" name="subscription_fees_discount" readonly>
                            </div>  
                            
                            <div class="form-group  col-md-12 fees hidden ">
                                <label class="control-label" for="name">Subscription Fees</label>
                                <input type="text" class="form-control" name="subscription_fees_total" readonly>
                            </div>  

                            <div class="form-group  col-md-12 package_name hidden ">
                                <label class="control-label" for="name">Package Name</label>
                                <select name="package" class="form-control packageName"  id="packageName">

                                </select>
                            </div>  

                            <div class="form-group  col-md-12 type_num_days hidden ">
                                <label class="control-label" for="name">Number Of Training Days</label>
                                <select class="form-control" name="type_num_days" id="type_num_days">
                                    <option value=""></option>
                                </select>
                            </div>     
                            
                            <div class="form-group  col-md-12 multi_classes hidden ">
                                <label class="control-label" for="name">classes</label>
                                <select class="form-control" class="select_classes" name="subscription_belongstomany_classe_relationship[]" id="select_classes" multiple="multiple">
                                    <option value=""></option>
                                </select>
                            </div> 
                            
                        </div><!-- panel-body -->
                        <div class="panel-body">
                            <button type="submit" class="btn btn-primary next">Next</button>
                        </div>
    
     
                        <button type="submit" class="btn btn-primary calculate hidden">Calculate</button>

                        <div class="panel-footer @if(!$edit) hidden @endif">
                            <button type="submit" class="btn btn-primary save">@if($edit) Save Changes @else Save Subscription @endif</button>
                                @yield('submit-buttons')
                                
                                                               
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script>
        var params = {};
        var $file;
        DaysArray = []
        selected = [];

        function deleteHandler(tag, isMulti) {
          return function() {
            $file = $(this).siblings(tag);

            params = {
                slug:   '{{ $dataType->slug }}',
                filename:  $file.data('file-name'),
                id:     $file.data('id'),
                field:  $file.parent().data('field-name'),
                multi: isMulti,
                _token: '{{ csrf_token() }}'
            }

            $('.confirm_delete_name').text(params.filename);
            $('#confirm_delete_modal').modal('show');
          };
        }

        let elem = [...document.querySelectorAll('.checkBox')]
        elem.forEach(item => item.addEventListener('change', getChecked))

        function getChecked() {
            DaysArray = elem.filter(item => item.checked).map(item => item.value)
            console.log(DaysArray)
        }
        
        $('document').ready(function () {
            
            @if($edit)
                $("input[name=subscriber]").replaceWith('<select id="subscribers" name="subscriber" class="form-control select2"><option  value="{{ $dataTypeContent->subscriber }}">{{ $dataTypeContent->subscriber }}</option></select>');
            @endif
            
            $("input[name=subscriber]").replaceWith('<select id="subscribers" name="subscriber" class="form-control select2">'+'</select>');
        
            $('#select_classes').on('select2:close', function (evt) {
                var uldiv = $(this).siblings('span.select2').find('ul')
                selected = uldiv.find('li').length - 1;
            });
            
            $("input[name=age]").removeAttr('value');
            $('#select_classes').select2();
            $('#subscribers').select2({tags: true});
            termss = []
            AvilableDays = []
            var package = 0;
            var x = 0;

            $('.next').on('click', function(e) {
                e.preventDefault(); 
                if ($("select[name=user_id]").val() == "") {
                    toastr.error("client is required");
                    return;
                }    
                if ($("input[name=subscriber]").val() == "") {
                    toastr.error("child name is required");
                    return;
                }                   
                if ($("input[name=academy]").val() == "") {
                    toastr.error("academy is required");
                    return;
                }     
                if ($("input[name=age]").val() == "") {
                    toastr.error("date of birth is required");
                    return;
                }                                                                         
                var year = $("input[name=age]").val();
                var academy = $("select[name=academy]").val();
                dob = new Date(year);
                var today = new Date();
                var my_age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));  
                $('select[name="subscription_belongstomany_term_relationship[]"]').each( function () {
                    termss.push($(this).val());
                });                
                var data = {
                    years : my_age,
                    terms : termss[0],
                    academy : $("select[name=academy]").val(),
                    city : $("select[name=city]").val(),
                }
                $.ajax({
                    url: '{{ url('api/v1/academy/packages/check') }}',
                    type: "POST",
                    data: data,
                    dataType: 'json',
                    success: function(response) {
                        console.log(response['data']);
                        if(response['data'] == null){
                            toastr.error("please select another date and terms");
                            return;
                        }else{
                            console.log(response['data']);
                            $('.custom_id').addClass("block");
                            $('.next').addClass("hidden");
                            $('.calculate').addClass("block");
      
                            $.each(response['classes'], function(key, value) {   
                                $('#select_classes').append($("<option></option>").attr("value", value.id).text(value.title)); 
                            }); 

                            var i = 0;
                            $.each(response['data'], function(key, value) {  
                                i++;
                                if(i == 1){
                                    package = value.id
                                    console.log(value);
                                    $('#packageName').append($("<option selected></option>").attr("value", value.id).text(value.package_name)); 
                                    if(value.day1_active == 1){
                                        AvilableDays.push('1')
                                    }
                                    if(value.day2_active == 1){
                                        AvilableDays.push('2')
                                    }
                                    if(value.day3_active == 1){
                                        this.day3_active = true
                                        AvilableDays.push('3')
                                    }
                                    if(value.day4_active == 1){
                                        this.day4_active = true
                                        AvilableDays.push('4')
                                    }
                                    if(value.day5_active == 1){
                                        this.day5_active = true
                                        AvilableDays.push('5')
                                    }
                                    if(value.day6_active == 1){
                                        this.day6_active = true
                                        AvilableDays.push('6')
                                    }
                                    if(value.day7_active == 1){
                                        this.day7_active = true
                                        AvilableDays.push('7')
                                    }                                    
                                }else{
                                    $('#packageName').append($("<option></option>").attr("value", value.id).text(value.package_name)); 
                                    
                                }
                            }); 


                            $('.package_name').removeClass("hidden");
                            $('.multi_classes').removeClass("hidden");
                            
                            if(AvilableDays.length == 1){
                               x = AvilableDays[0];
                            }else{
                                $('.type_num_days').removeClass("hidden");
                                $.each(AvilableDays, function(key, value) {   
                                    $('#type_num_days').append($("<option></option>").attr("value", value).text(value)); 
                                });                                
                            }
                        } 

                    }
                });                

            });
            
            $("select[name=user_id]").change(function() {
                $('#subscribers option').remove();
                 var id = $(this).find('option:selected').val();
                 var data = {id : id}
                $.ajax({
                    url: '{{ url('api/v1/user/show') }}',
                    type: "POST",
                    data: data,
                    dataType: 'json',
                    success: function(response) {
                        console.log(response);
                        console.log(response['data']['children']);
                    $.each(response['data']['children'], function(key, value) {   
                        $('#subscribers').append($("<option></option>").attr("value", value.id).text(value.name)); 
                    });                          
                       
                    }
                }); 
            });

            $('#type_num_days').change(function() {
                var $option = $(this).find('option:selected');
                x = $option.val();//to get content of "value" attrib
            });

            $('#packageName').change(function() {
                console.log(AvilableDays)
                package = $(this).find('option:selected').val();
                console.log(package);
                $('#type_num_days option').remove();
                var data = {id : package}
                $.ajax({
                    url: '{{ url('api/v1/academy/package') }}',
                    type: "POST",
                    data: data,
                    dataType: 'json',
                    success: function(response) {
                        console.log(response);
                        AvilableDays = [];
                        if(response['data']['day1_active'] == 1){
                            AvilableDays.push('1')
                        }
                        if(response['data']['day2_active'] == 1){
                            AvilableDays.push('2')
                        }
                        if(response['data']['day3_active'] == 1){
                            this.day3_active = true
                            AvilableDays.push('3')
                        }
                        if(response['data']['day4_active'] == 1){
                            this.day4_active = true
                            AvilableDays.push('4')
                        }
                        if(response['data']['day5_active'] == 1){
                            this.day5_active = true
                            AvilableDays.push('5')
                        }
                        if(response['data']['day6_active'] == 1){
                            this.day6_active = true
                            AvilableDays.push('6')
                        }
                        if(response['data']['day7_active'] == 1){
                            this.day7_active = true
                            AvilableDays.push('7')
                        }     
                        
                        
                        if(AvilableDays.length == 1){
                           x = AvilableDays[0];
                        }else{
                            $.each(AvilableDays, function(key, value) {   
                                $('#type_num_days')
                                    .append($("<option></option>")
                                                .attr("value", value)
                                                .text(value)); 
                            });                                
                        }
                        
                            
                    }
                });

            });

            $('.calculate').on('click', function(e) {
                e.preventDefault();
                console.log("calculate package"+package);
                var startDate = new Date(Date.parse($("input[name=start_date]").val()));
                var TodayDate = new Date();

                console.log(startDate);
                    if(selected != x){
                        toastr.error("you must select "+x+" Class");
                        return;
                    }
                    if ($("input[name=start_date]").val() == "") {
                        toastr.error("start date is required");
                        return;
                    }   
     
                    if( startDate < TodayDate ) {
                        toastr.error("start date must larg today");
                        return;
                    }  
                    var data = {
                    name : $("input[name=subscriber]").val(),
                    num_day : selected,
                    package : package,
                    sport : $("input[name=academy]").val(),
                    startDate : $("input[name=start_date]").val(),
                    age : $("input[name=age]").val(),
                    terms : termss[0],
                    user : $("select[name=user_id]").val(),
                }                
                $.ajax({
                    url: '{{ url('api/v1/academy/calculate/amount') }}',
                    type: "POST",
                    data: data,
                    dataType: 'json',
                    success: function(response) {
                        console.log(response);
                        $("input[name=subscription_fees]").val(response['totalwithdiscount']),
                        $("input[name=subscription_fees_discount]").val(response['discount']),
                        $("input[name=subscription_fees_total]").val(response['cost']),
                        $('.fees').addClass("block");
                        $('.custom_id2').addClass("block");
                        $('.calculate').removeClass("block");
                        $('.panel-footer ').addClass("block");
                    }
                });                 
            });
            
            // $('.save').on('click', function(e) {
            //     e.preventDefault();
            //     $.ajax({
            //         url: '{{ '/admin/subscriptions/store' }}',
            //         type: "PUT",
            //         data: $("#formdata").serialize(),
            //         dataType: 'json',
            //         success: function(response) {
            //             console.log(response);
            //             console.log(response.state);
            //             if (response.state == 200) {
            //                 window.location.href = "{{ url('admin/bookings') }}";
            //                 toastr.success(response.msg);
            //             } else {
            //                 $(':input[type="submit"]').prop('enable', true);
            //                 toastr.error(response.msg);
            //             }
            //         }
            //     });
            // });

            $('.toggleswitch').bootstrapToggle();


            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                } else if (elt.type != 'date') {
                    elt.type = 'text';
                    $(elt).datetimepicker({
                        format: 'L',
                        extraFormats: [ 'YYYY-MM-DD' ]
                    }).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));
            $('.form-group').on('click', '.remove-single-image', deleteHandler('img', false));
            $('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));
            $('.form-group').on('click', '.remove-single-file', deleteHandler('a', false));

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.'.$dataType->slug.'.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $file.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing file.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@stop
