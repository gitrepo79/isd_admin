@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->getTranslatedAttribute('display_name_plural'))

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i> {{ $dataType->getTranslatedAttribute('display_name_plural') }}
        </h1>
        @can('add', app($dataType->model_name))
            <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success btn-add-new">
                <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
            </a>
        @endcan
        @can('delete', app($dataType->model_name))
            @include('voyager::partials.bulk-delete')
        @endcan
        @can('edit', app($dataType->model_name))
            @if(isset($dataType->order_column) && isset($dataType->order_display_column))
                <a href="{{ route('voyager.'.$dataType->slug.'.order') }}" class="btn btn-primary btn-add-new">
                    <i class="voyager-list"></i> <span>{{ __('voyager::bread.order') }}</span>
                </a>
            @endif
        @endcan
        <a href="{{ url('admin/subscriptions') }}?not_paid=1" class="btn btn-warning btn-add-new">
            <i class="voyager-plus"></i> <span>Show Unpaid Subscriptions</span>
        </a>
        
        <a href="{{ url('admin/subscriptions') }}?paid=1" class="btn btn-warning btn-add-new">
            <i class="voyager-plus"></i> <span>Show paid Subscriptions</span>
        </a>
    
    
        <a href="{{ url('admin/subscriptions') }}?saved=1" class="btn btn-warning btn-add-new">
            <i class="voyager-plus"></i> <span>Show Saved Subscriptions</span>
        </a>
        
        <a href="{{ url('admin/subscriptions') }}?published=1" class="btn btn-warning btn-add-new">
            <i class="voyager-plus"></i> <span>Show Published Subscriptions</span>
        </a>
        @can('delete', app($dataType->model_name))
            @if($usesSoftDeletes)
                <input type="checkbox" @if ($showSoftDeleted) checked @endif id="show_soft_deletes" data-toggle="toggle" data-on="{{ __('voyager::bread.soft_deletes_off') }}" data-off="{{ __('voyager::bread.soft_deletes_on') }}">
            @endif
        @endcan
        @foreach($actions as $action)
            @if (method_exists($action, 'massAction'))
                @include('voyager::bread.partials.actions', ['action' => $action, 'data' => null])
            @endif
        @endforeach
        @include('voyager::multilingual.language-selector')

        <form action="" method="get">
            {{-- <div class="col-sm-4">
                <label for="staticEmail" class="col-sm-3 col-form-label">User</label>
                <div class="col-sm-9">
                    <?php $users = DB::table('users')->where('role_id', 2)->get(); ?>
                    <select class="form-control input-sm" name="users">
                        <option value="">Select User</option>
                        @foreach ($users as $task)

                            <option value="{{ $task->id }}" @if (isset($_GET['users']) && $_GET['users'] > 0)
                                @if ($task->id == $_GET['users']) selected @endif
                        @endif>{{ $task->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div> --}}
            <div class="col-sm-4">
                <label for="staticEmail" class="col-sm-3 col-form-label">Age Group</label>
                <div class="col-sm-9">
                    <?php $ages = DB::table('ages')->get(); ?>
                    <select class="form-control input-sm" name="age">
                        <option value="">Select Age Group</option>
                        @foreach ($ages as $task)
                            <option value="{{ $task->id }}" @if (isset($_GET['age']) && $_GET['age'] > 0)
                                @if ($task->id == $_GET['age']) selected @endif
                        @endif>{{ $task->title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <label for="staticEmail" class="col-sm-3 col-form-label">Academy</label>
                <div class="col-sm-9">
                    <?php $academies = DB::table('academies')->get(); ?>
                    <select class="form-control input-sm" name="academy">
                        <option value="">Select Academy</option>
                        @foreach ($academies as $task)
                            <option value="{{ $task->id }}" @if (isset($_GET['academy']) && $_GET['academy'] > 0)
                                @if ($task->id == $_GET['academy']) selected @endif
                        @endif>{{ $task->title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <label for="staticEmail" class="col-sm-3 col-form-label">Subscriber</label>
                <div class="col-sm-9">
                    <?php $subscriber = DB::table('users')->where('type', 2)->get(); ?>
                    <select class="form-control input-sm" name="subscriber">
                        <option value="">Select Subscriber Name</option>
                        @foreach ($subscriber as $task)
                            <option value="{{ $task->id }}" @if (isset($_GET['subscriber']) && $_GET['subscriber'] > 0)
                                @if ($task->id == $_GET['subscriber']) selected @endif
                        @endif>{{ $task->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>    
            <div class="col-sm-4">
                <label for="staticEmail" class="col-sm-3 col-form-label">Teams</label>
                <div class="col-sm-9">
                    <?php $teams = DB::table('teams')->get(); ?>
                    <select class="form-control input-sm" name="team">
                        <option value="">Select Team</option>
                        @foreach ($teams as $task)
                            <option value="{{ $task->id }}" @if (isset($_GET['team']) && $_GET['team'] > 0)
                                @if ($task->id == $_GET['team']) selected @endif
                        @endif>{{ $task->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>    
            <div class="col-sm-4">
                <label for="staticEmail" class="col-sm-3 col-form-label">Terms</label>
                <div class="col-sm-9">
                    <?php $terms = DB::table('terms')->get(); ?>
                    <select class="form-control input-sm select2" multiple="multiple" name="term[]">
                        <option value="">Select Term</option>
                        @foreach ($terms as $task)
                            <option value="{{ $task->id }}" @if (isset($_GET['term']) && $_GET['term'] > 0)
                                @if ($task->id == $_GET['term']) selected @endif
                        @endif>{{ $task->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>   
            <div class="col-sm-4">
                <label for="staticEmail" class="col-sm-3 col-form-label">Packages</label>
                <div class="col-sm-9">
                    <?php $packages = DB::table('packages')->get(); ?>
                    <select class="form-control input-sm" name="package">
                        <option value="">Select Package Name</option>
                        @foreach ($packages as $task)
                            <option value="{{ $task->id }}" @if (isset($_GET['package']) && $_GET['package'] > 0)
                                @if ($task->id == $_GET['package']) selected @endif
                        @endif>{{ $task->package_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>    
            <div class="col-sm-4">
                <label for="staticEmail" class="col-sm-3 col-form-label">Training Times</label>
                <div class="col-sm-9">
                    <?php $class = DB::table('class')->get(); ?>
                    <select class="form-control input-sm" name="class">
                        <option value="">Select Training Time Name</option>
                        @foreach ($class as $task)
                            <option value="{{ $task->id }}" @if (isset($_GET['class']) && $_GET['class'] > 0)
                                @if ($task->id == $_GET['class']) selected @endif
                        @endif>{{ $task->title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>     
            <div class="col-sm-4">
                <label for="staticEmail" class="col-sm-3 col-form-label">Levels</label>
                <div class="col-sm-9">
                    <?php $levels = DB::table('levels')->get(); ?>
                    <select class="form-control input-sm" name="level">
                        <option value="">Select Level</option>
                        @foreach ($levels as $task)
                            <option value="{{ $task->id }}" @if (isset($_GET['level']) && $_GET['level'] > 0)
                                @if ($task->id == $_GET['level']) selected @endif
                        @endif>{{ $task->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>  
            <div class="col-sm-4">
                <label for="staticEmail" class="col-sm-3 col-form-label">Training Days</label>
                <div class="col-sm-9">
                    <?php $subscriber = DB::table('users')->where('type', 2)->get(); ?>
                    <select class="form-control input-sm" name="num_days">
                        <option value="">Select Training Day</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>

                    </select>
                </div>
            </div>                                                                     
            <div class="col-sm-4">
                <button type="submit" class="btn btn-primary">Search</button>
            </div>
        </form>

    </div>
@stop

@section('content')
<style>
    .dropdown, .dropup{
        list-style-type: none; 
    } 
    .dropdown-menu a {
        float: left !important;
        clear: both;
        margin: 0 !important;
        width: 100%;
        border-radius: 0 !important;
    }   
    .open>.dropdown-menu {
        display: block;
        min-width: 120px !important;
        padding: 0 !important;
         margin: 0 !important;     
         left: -40px;   
    }
    .panel-bordered>.panel-body {
    overflow: inherit !important;
    }    
    .table-responsive {
        overflow-x: clip !important;
    }

        .col-sm-4 {
            height: 35px;
            margin-bottom: 10px;
        }    
        </style>
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        @if ($isServerSide)
                            <form method="get" class="form-search">
                                <div id="search-input">
                                    <div class="col-2">
                                        <select id="search_key" name="key">
                                            @foreach($searchNames as $key => $name)
                                                <option value="{{ $key }}" @if($search->key == $key || (empty($search->key) && $key == $defaultSearchKey)) selected @endif>{{ $name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-2">
                                        <select id="filter" name="filter">
                                            <option value="contains" @if($search->filter == "contains") selected @endif>contains</option>
                                            <option value="equals" @if($search->filter == "equals") selected @endif>=</option>
                                        </select>
                                    </div>
                                    <div class="input-group col-md-12">
                                        <input type="text" class="form-control" placeholder="{{ __('voyager::generic.search') }}" name="s" value="{{ $search->value }}">
                                        <span class="input-group-btn">
                                            <button class="btn btn-info btn-lg" type="submit">
                                                <i class="voyager-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                                @if (Request::has('sort_order') && Request::has('order_by'))
                                    <input type="hidden" name="sort_order" value="{{ Request::get('sort_order') }}">
                                    <input type="hidden" name="order_by" value="{{ Request::get('order_by') }}">
                                @endif
                            </form>
                        @endif
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                    <tr>
                                        @if($showCheckboxColumn)
                                            <th class="dt-not-orderable">
                                                <input type="checkbox" class="select_all">
                                            </th>
                                        @endif
                                        @foreach($dataType->browseRows as $row)
                                        <th>
                                            @if ($isServerSide && $row->type !== 'relationship')
                                                <a href="{{ $row->sortByUrl($orderBy, $sortOrder) }}">
                                            @endif
                                            {{ $row->getTranslatedAttribute('display_name') }}
                                            @if ($isServerSide)
                                                @if ($row->isCurrentSortField($orderBy))
                                                    @if ($sortOrder == 'asc')
                                                        <i class="voyager-angle-up pull-right"></i>
                                                    @else
                                                        <i class="voyager-angle-down pull-right"></i>
                                                    @endif
                                                @endif
                                                </a>
                                            @endif
                                        </th>
                                        @endforeach
                                        <th class="actions text-right dt-not-orderable">{{ __('voyager::generic.actions') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($dataTypeContent as $data)
                                    <tr>
                                        @if($showCheckboxColumn)
                                            <td>
                                                <input type="checkbox" name="row_id" id="checkbox_{{ $data->getKey() }}" value="{{ $data->getKey() }}">
                                            </td>
                                        @endif
                                        @foreach($dataType->browseRows as $row)
                                            @php
                                            if ($data->{$row->field.'_browse'}) {
                                                $data->{$row->field} = $data->{$row->field.'_browse'};
                                            }
                                            @endphp
                                            <td>
                                                @if (isset($row->details->view))
                                                    @include($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $data->{$row->field}, 'action' => 'browse', 'view' => 'browse', 'options' => $row->details])
                                                @elseif($row->type == 'image')
                                                    <img src="@if( !filter_var($data->{$row->field}, FILTER_VALIDATE_URL)){{ Voyager::image( $data->{$row->field} ) }}@else{{ $data->{$row->field} }}@endif" style="width:100px">
                                                @elseif($row->type == 'relationship')
                                                    @include('voyager::formfields.relationship', ['view' => 'browse','options' => $row->details])
                                                @elseif($row->type == 'select_multiple')
                                                    @if(property_exists($row->details, 'relationship'))

                                                        @foreach($data->{$row->field} as $item)
                                                            {{ $item->{$row->field} }}
                                                        @endforeach

                                                    @elseif(property_exists($row->details, 'options'))
                                                        @if (!empty(json_decode($data->{$row->field})))
                                                            @foreach(json_decode($data->{$row->field}) as $item)
                                                                @if (@$row->details->options->{$item})
                                                                    {{ $row->details->options->{$item} . (!$loop->last ? ', ' : '') }}
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            {{ __('voyager::generic.none') }}
                                                        @endif
                                                    @endif

                                                    @elseif($row->type == 'multiple_checkbox' && property_exists($row->details, 'options'))
                                                        @if (@count(json_decode($data->{$row->field})) > 0)
                                                            @foreach(json_decode($data->{$row->field}) as $item)
                                                                @if (@$row->details->options->{$item})
                                                                    {{ $row->details->options->{$item} . (!$loop->last ? ', ' : '') }}
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            {{ __('voyager::generic.none') }}
                                                        @endif

                                                @elseif(($row->type == 'select_dropdown' || $row->type == 'radio_btn') && property_exists($row->details, 'options'))

                                                    {!! $row->details->options->{$data->{$row->field}} ?? '' !!}

                                                @elseif($row->type == 'date' || $row->type == 'timestamp')
                                                    @if ( property_exists($row->details, 'format') && !is_null($data->{$row->field}) )
                                                        {{ \Carbon\Carbon::parse($data->{$row->field})->formatLocalized($row->details->format) }}
                                                    @else
                                                        {{ $data->{$row->field} }}
                                                    @endif
                                                @elseif($row->type == 'checkbox')
                                                    @if(property_exists($row->details, 'on') && property_exists($row->details, 'off'))
                                                        @if($data->{$row->field})
                                                            <span class="label label-info">{{ $row->details->on }}</span>
                                                        @else
                                                            <span class="label label-primary">{{ $row->details->off }}</span>
                                                        @endif
                                                    @else
                                                    {{ $data->{$row->field} }}
                                                    @endif
                                                @elseif($row->type == 'color')
                                                    <span class="badge badge-lg" style="background-color: {{ $data->{$row->field} }}">{{ $data->{$row->field} }}</span>
                                                @elseif($row->type == 'text')
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    <div>{{ mb_strlen( $data->{$row->field} ) > 200 ? mb_substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}</div>
                                                @elseif($row->type == 'text_area')
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    <div>{{ mb_strlen( $data->{$row->field} ) > 200 ? mb_substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}</div>
                                                @elseif($row->type == 'file' && !empty($data->{$row->field}) )
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    @if(json_decode($data->{$row->field}) !== null)
                                                        @foreach(json_decode($data->{$row->field}) as $file)
                                                            <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($file->download_link) ?: '' }}" target="_blank">
                                                                {{ $file->original_name ?: '' }}
                                                            </a>
                                                            <br/>
                                                        @endforeach
                                                    @else
                                                        <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($data->{$row->field}) }}" target="_blank">
                                                            Download
                                                        </a>
                                                    @endif
                                                @elseif($row->type == 'rich_text_box')
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    <div>{{ mb_strlen( strip_tags($data->{$row->field}, '<b><i><u>') ) > 200 ? mb_substr(strip_tags($data->{$row->field}, '<b><i><u>'), 0, 200) . ' ...' : strip_tags($data->{$row->field}, '<b><i><u>') }}</div>
                                                @elseif($row->type == 'coordinates')
                                                    @include('voyager::partials.coordinates-static-image')
                                                @elseif($row->type == 'multiple_images')
                                                    @php $images = json_decode($data->{$row->field}); @endphp
                                                    @if($images)
                                                        @php $images = array_slice($images, 0, 3); @endphp
                                                        @foreach($images as $image)
                                                            <img src="@if( !filter_var($image, FILTER_VALIDATE_URL)){{ Voyager::image( $image ) }}@else{{ $image }}@endif" style="width:50px">
                                                        @endforeach
                                                    @endif
                                                @elseif($row->type == 'media_picker')
                                                    @php
                                                        if (is_array($data->{$row->field})) {
                                                            $files = $data->{$row->field};
                                                        } else {
                                                            $files = json_decode($data->{$row->field});
                                                        }
                                                    @endphp
                                                    @if ($files)
                                                        @if (property_exists($row->details, 'show_as_images') && $row->details->show_as_images)
                                                            @foreach (array_slice($files, 0, 3) as $file)
                                                            <img src="@if( !filter_var($file, FILTER_VALIDATE_URL)){{ Voyager::image( $file ) }}@else{{ $file }}@endif" style="width:50px">
                                                            @endforeach
                                                        @else
                                                            <ul>
                                                            @foreach (array_slice($files, 0, 3) as $file)
                                                                <li>{{ $file }}</li>
                                                            @endforeach
                                                            </ul>
                                                        @endif
                                                        @if (count($files) > 3)
                                                            {{ __('voyager::media.files_more', ['count' => (count($files) - 3)]) }}
                                                        @endif
                                                    @elseif (is_array($files) && count($files) == 0)
                                                        {{ trans_choice('voyager::media.files', 0) }}
                                                    @elseif ($data->{$row->field} != '')
                                                        @if (property_exists($row->details, 'show_as_images') && $row->details->show_as_images)
                                                            <img src="@if( !filter_var($data->{$row->field}, FILTER_VALIDATE_URL)){{ Voyager::image( $data->{$row->field} ) }}@else{{ $data->{$row->field} }}@endif" style="width:50px">
                                                        @else
                                                            {{ $data->{$row->field} }}
                                                        @endif
                                                    @else
                                                        {{ trans_choice('voyager::media.files', 0) }}
                                                    @endif
                                                @else
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    <span>{{ $data->{$row->field} }}</span>
                                                @endif
                                            </td>
                                        @endforeach
                                        <td class="no-sort no-click bread-actions">
                                            <li class="dropdown control">
                                                <a href="#" class="dropdown-toggle btn btn-info btn-sm" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="    width: auto;">
                                                    view more <i class="fa fa-chevron-circle-down ml-1"></i>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <?php $amount = DB::table('payment')->where('book_id', $data->id)->where('rent', 2)->sum('amount'); $amount = round($amount, 2) ?>
                                                    @foreach($actions as $action)
                                                        @if (!method_exists($action, 'massAction'))
                                                            @include('voyager::bread.partials.actions', ['action' => $action])
                                                        @endif
                                                    @endforeach
                                                    @if ($amount < $data->subscription_fees)
                                                        <a class="btn pay btn-sm btn-dark pull-right view"
                                                            data-id="{{ $data->id }}">
                                                            <i class="voyager-credit-cards"></i> <span class="hidden-xs hidden-sm">Pay</span>
                                                        </a>
                                                    @endif 
                                                    <a class="btn btn-sm btn-primary pull-right view" href="{{ url('export/receipt/subscription',$data->id)}}" > <i class="voyager-credit-cards"></i> <span class="hidden-xs hidden-sm">Invoice</span></a>
                                                </ul>
                                            </li>                                            
                                        </td>
                                    </tr>
                                    <div class="modal fade " id="pay_modal{{ $data->id }}" tabindex="-1"
                                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Add Payment
                                                        Information</h5>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="{{ url('add/pay') }}" method="post">
                                                        @csrf
                                                        <div class="mb-3">
                                                            <label class="form-label">Amount already paid</label>
                                                            <?php $paid = DB::table('payment')->where('book_id', $data->id)->where('rent', 2)->sum('amount'); 
                                                                  $paid = round($paid, 2); ?>
                                                            <input type="text" class="form-control"
                                                                value="{{ $paid }}" readonly>
                                                            <input type="hidden" name="rent" value="2">
                                                        </div>
                                                        <div class="mb-3">
                                                            <label class="form-label">Payable Amount</label>
                                                            <input type="text" class="form-control" name="amount"
                                                                value="{{ $data->subscription_fees - $paid }}">
                                                        </div>
                                                        <div class="mb-3">
                                                            <label class="form-label">Type</label>
                                                            <select class="form-control" name="type">
                                                                <option value="1">Cash</option>
                                                                <option value="2">Bank Transfer</option>
                                                                <option value="3">Client wallet</option>
                                                                <option value="4">Credit Card</option>
                                                                <option value="5">Cash / Credit Card</option>
                                                            </select>
                                                        </div>
                                                        <div class="mb-3">
                                                            <label class="form-label">Transaction Authentication
                                                                Code</label>
                                                            <input type="text" class="form-control" name="ref">
                                                        </div>
                                                        <input type="hidden" name="book_id" value="{{ $data->id }}">
                                                        <input type="hidden" name="client_id" value="{{$data->user_id}}">  
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-secondary"
                                                                data-bs-dismiss="modal"
                                                                data-dismiss="modal">Close</button>
                                                            <button type="submit" class="btn btn-primary">Register Payment</button>
                                                        </div>
                                                    </form>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @if ($isServerSide)
                            <div class="pull-left">
                                <div role="status" class="show-res" aria-live="polite">{{ trans_choice(
                                    'voyager::generic.showing_entries', $dataTypeContent->total(), [
                                        'from' => $dataTypeContent->firstItem(),
                                        'to' => $dataTypeContent->lastItem(),
                                        'all' => $dataTypeContent->total()
                                    ]) }}</div>
                            </div>
                            <div class="pull-right">
                                {{ $dataTypeContent->appends([
                                    's' => $search->value,
                                    'filter' => $search->filter,
                                    'key' => $search->key,
                                    'order_by' => $orderBy,
                                    'sort_order' => $sortOrder,
                                    'showSoftDeleted' => $showSoftDeleted,
                                ])->links() }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ strtolower($dataType->getTranslatedAttribute('display_name_singular')) }}?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_confirm') }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('css')
@if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
    <link rel="stylesheet" href="{{ voyager_asset('lib/css/responsive.dataTables.min.css') }}">
@endif
@stop

@section('javascript')

<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

    <!-- DataTables -->
    @if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
        <script src="{{ voyager_asset('lib/js/dataTables.responsive.min.js') }}"></script>
    @endif
    <script>
        $(document).ready(function () {

            $('select').select2();

            @if (!$dataType->server_side)
            var table = $('#dataTable').DataTable({
                dom: 'Bfrtip',
                buttons: ['copy', 'csv', 'excel', 'pdf', 'print']
                });
            @else
                $('#search-input select').select2({
                    minimumResultsForSearch: Infinity
                });
            @endif

            @if ($isModelTranslatable)
                $('.side-body').multilingual();
                //Reinitialise the multilingual features when they change tab
                $('#dataTable').on('draw.dt', function(){
                    $('.side-body').data('multilingual').init();
                })
            @endif
            $('.select_all').on('click', function(e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked')).trigger('change');
            });
        });


        var deleteFormAction;
        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '{{ route('voyager.'.$dataType->slug.'.destroy', '__id') }}'.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });
        $('td').on('click', '.pay', function(e) {
            var id = $(this).attr("data-id")
            console.log(id);
            $('#pay_modal' + id).modal('show');
        });
        @if($usesSoftDeletes)
            @php
                $params = [
                    's' => $search->value,
                    'filter' => $search->filter,
                    'key' => $search->key,
                    'order_by' => $orderBy,
                    'sort_order' => $sortOrder,
                ];
            @endphp
            $(function() {
                $('#show_soft_deletes').change(function() {
                    if ($(this).prop('checked')) {
                        $('#dataTable').before('<a id="redir" href="{{ (route('voyager.'.$dataType->slug.'.index', array_merge($params, ['showSoftDeleted' => 1]), true)) }}"></a>');
                    }else{
                        $('#dataTable').before('<a id="redir" href="{{ (route('voyager.'.$dataType->slug.'.index', array_merge($params, ['showSoftDeleted' => 0]), true)) }}"></a>');
                    }

                    $('#redir')[0].click();
                })
            })
        @endif
        $('input[name="row_id"]').on('change', function () {
            var ids = [];
            $('input[name="row_id"]').each(function() {
                if ($(this).is(':checked')) {
                    ids.push($(this).val());
                }
            });
            $('.selected_ids').val(ids);
        });
    </script>
@stop
